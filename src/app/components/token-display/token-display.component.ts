import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-token-display',
  templateUrl: './token-display.component.html',
  styleUrls: ['./token-display.component.scss']
})
export class TokenDisplayComponent implements OnInit {
  @Input() tokens: Array<string>;

  constructor() { }

  ngOnInit() {
  }

}
