import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TokenService } from '../../services/token.service';


@Component({
  selector: 'app-token-receive',
  templateUrl: './token-receive.component.html',
  styleUrls: ['./token-receive.component.scss']
})
export class TokenReceiveComponent implements OnInit {
  tokens: Array<string>;
  @Output() didReceiveToken = new EventEmitter<Array<string>>();

  constructor(private tokenService: TokenService) {
  }

  ngOnInit() {
    this.tokens = this.initialiseTokensArray(6);
    console.log('TokenReceive: ngOnInit()')
    setInterval(() => {
      this.generateToken();
    }, 3000)
  }

  initialiseTokensArray(n) {
    const arr = Array.apply(null, Array(n));
    return arr.map(function (x, i) { return '' });
  }

  generateToken() {
    this.tokenService.getToken({name: 'Robert Britton', age: '31', occupation: 'Software Developer'})
    .subscribe(response => {
        this.tokens.unshift(response['token']);
        this.tokens.pop();
        this.didReceiveToken.emit(this.tokens);
    })
  }
}
