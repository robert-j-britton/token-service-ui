import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { TokenService } from './services/token.service';
import { TokenDisplayComponent } from './components/token-display/token-display.component';
import { TokenReceiveComponent } from './components/token-receive/token-receive.component';


@NgModule({
  declarations: [
    AppComponent,
    TokenDisplayComponent,
    TokenReceiveComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [TokenService],
  bootstrap: [AppComponent]
})
export class AppModule { }
