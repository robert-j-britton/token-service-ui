import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  tokens: Array<string>;

  onDidReceiveTokenHandler(event: Array<string>) {
    this.tokens = event;
  }
}
