import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class TokenService {

  constructor(private http: HttpClient) { }

  getToken(payload: any): Observable<string> {
    return this.http.post<string>('http://localhost:4000/api/token/generate/', payload)
  }

  getTokenSplit(payload: any): Observable<string> {
    return this.http.post<string>('http://localhost:4000/api/token/generate/split', payload)
  }
}
